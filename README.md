# Onlinehandel zum Unterstuetzen

Eine Sammlung von Handelsunternehmen, die Unterstuetzung verdient haben.
Geboren ist diese Sammlung aus der Situation der C-19-Krise.

## Elektronkik

| Name | Webseite | Region | Anmerkung/Grund warum man sie unterstützen soll |
|------|----------|--------|-------------------------------------------------|
| -    | -        | -      |                                                 |


## Lebensmittel

| Name               | Webseite                                         | Was?                                       | Region         | Anmerkung/Grund warum man sie unterstützen soll |
|--------------------|--------------------------------------------------|--------------------------------------------|----------------|-------------------------------------------------|
| Evas Teeplantage   | https://www.evas-teeplantage.de/                 | „richtig guter Tee“                        | Bayern         | „richtig guter Tee“                             |
| Rübenretter        | https://www.ruebenretter.de/                     | regionales Gemüse                          | Bayern         |                                                 |
| Plum's Kaffee      | https://www.plumskaffee.de/de/Kaffee---Espresso/ | Kaffee                                     | Aachen         |                                                 |
| Schokoladen Outlet | https://www.schokoladen-outlet.de/               | Stark reduziete Bio-Schokolade, auch vegan | Castrop-Rauxel |                                                 |

## Kleidung

| Name     | Webseite                              | Was?                    | Region | Anmerkung/Grund warum man sie unterstützen soll |
|----------|---------------------------------------|-------------------------|--------|-------------------------------------------------|
| Manomama | https://www.manomama.de/wir-ueber-uns | Bio-/Fairtrade Kleidung |        | starkes soziales Engagement der Gründerin       |

## Medien/Unterhaltung

| Name           | Webseite                 | Was?                                                       | Region | Anmerkung/Grund warum man sie unterstützen soll |
|----------------|--------------------------|------------------------------------------------------------|--------|-------------------------------------------------|
| Just for Kicks | https://justforkicks.de/ | Progressive Rock, Progressive Metal, Artrock und ähnliches |        |                                                 |

## Medizinprodukte
| Name                            | Webseite                  | Was?                            | Region  | Anmerkung/Grund warum man sie unterstützen soll      |
|---------------------------------|---------------------------|---------------------------------|---------|------------------------------------------------------|
| KLEINE HUMMEL - STICKEREI WEITZ | www.kleine-hummel.de      | Masken für 10€/St. inlc. Spende | Leipzig |                                                      |
| urbandoo                        | https://www.urbandoo.net/ | medizinsch aktive Schals        |         | Mitgegründet von Sina Trinkwalder (siehe „Manomama“) |
|                                 |                           |                                 |         |                                                      |

# Andere Sammlungen dieser Art

https://wir-bestellen-hier.de/
